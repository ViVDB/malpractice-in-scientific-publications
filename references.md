# main subject

scientific publication in iran

https://iranian-studies.stanford.edu/sites/g/files/sbiybj6191/f/publications/the_scientific_output_of_iran-quantity_quality_corruption.pdf

scientific publication in India

https://www.nature.com/articles/d41586-019-00514-1


======

# éloigné du sujet :

https://www.elsevier.com/connect/7-steps-to-publishing-in-a-scientific-journal

https://retractionwatch.com/help-us-heres-some-of-what-were-working-on/

Europe as a publication system

https://link.springer.com/article/10.1023/A:1005639026643

publication bias (non-voluntary):

https://link.springer.com/article/10.1007/BF01173636

cit-net explorer :

http://ceur-ws.org/Vol-1143/paper2.pdf

http://www.citnetexplorer.nl/

competition in science :

https://www.jstor.org/stable/2094272?seq=1#page_scan_tab_contents

conflict of interest :

https://www.elsevier.com/__data/assets/pdf_file/0010/92476/ETHICS_COI02.pdf


-----

# important

in-depth analysis

https://www.pnas.org/content/pnas/109/42/17028.full.pdf

peer-review necessary

https://jamanetwork.com/journals/jama/article-abstract/376099

Peer-Review Fraud — Hacking the Scientific Publication Process

https://www.nejm.org/doi/full/10.1056/NEJMp1512330

Link between altmetric and fraud type

http://altmetrics.org/wp-content/uploads/2018/09/altmetrics18_paper_3_Shema_new.pdf

Authorship usurpation

https://onlinelibrary.wiley.com/doi/pdf/10.1046/j.1442-2026.2003.00432.x

old fraud (1981)

https://tauruspet.med.yale.edu/staff/edm42/IUPUI-website/emorris.tar/emorris/emorris/Ethics%20Course%2009/Journal%20articles/325207a0-Feder%20and%20Steward%20comment%20re%20Darsee%20co-authors%20and%20fraud%20Nature%201987.pdf

Does normal preparation and review discover fraud

https://www.sciencedirect.com/science/article/pii/S1383574204000547

authorship good pratices and guidelines

https://www.sciencedirect.com/science/article/pii/S1383574204000559

irresponsible authorship and wasteful publication

http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.844.664&rep=rep1&type=pdf

p-hacking detection

https://www.quora.com/What-are-the-ways-to-detect-p-hacking-How-can-journals-put-procedures-in-place-to-prevent-it-from-happening/answer/Christie-Aschwanden?share=99a1295f

predatory publishing

https://www.sciencedirect.com/science/article/pii/S1878875017308598

Truth or Consequences: The Growing Trend of Publication Retraction

https://www.sciencedirect.com/science/article/pii/S1878875017305818

anonymous peer review

https://www.sciencedirect.com/science/article/pii/S1878875017314389

fraud in neurosurgial publications, world

https://www.sciencedirect.com/science/article/pii/S1878875017305090

-----

# Indisponible :( but still important

https://search.proquest.com/openview/7f555613e798a3de8d56360cc810139e/1?pq-origsite=gscholar&cbl=1819046
